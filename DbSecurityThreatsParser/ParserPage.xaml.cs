﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.IO;

namespace DbSecurityThreatsParser
{
    public partial class ParserPage : Page
    {
        static string strURL = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        static string strDirPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        static string fileName = "thrlist.xlsx";
        static string strFilePath = System.IO.Path.Combine(strDirPath, fileName);
        static string strTempDir = @"C:\temp";
        static string strTempFilePath = System.IO.Path.Combine(strTempDir, fileName);
        static string strError = "";
        DataTable localFile = null;

        public ParserPage()
        {
            InitializeComponent();            
        }

        private void ParserPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OperationsWithFiles.CheckExistFile(strDirPath))
                {
                    infoListBox.Items.Add("- Local file with saved information found!");
                    localFile = ImportFromExcel.ImportExcelFile(strFilePath);
                    threatSearchCbx.ItemsSource = localFile.DefaultView;
                    threatSearchCbx.DisplayMemberPath = localFile.Columns[1].ColumnName;
                    fullInfoNameLbx.Visibility = Visibility.Hidden;
                }
                else infoListBox.Items.Add("- Local file with saved information not found! Please click download button!");
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }            
        }

        private void DownloadBtn_Click(object sender, RoutedEventArgs e)
        {
            fullInfoNameLbx.Visibility = Visibility.Hidden;
            threatsDataGrid.Visibility = Visibility.Visible;
            try
            {
                if (!DownloadFileFromWebsite.DoWebRequest(strURL, strDirPath, ref strError))
                {
                    infoListBox.Items.Add("- Could not download!Error: " + strError);
                }
                else infoListBox.Items.Add("- File is successfully downloaded!");
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }            
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            fullInfoNameLbx.Visibility = Visibility.Hidden;
            threatsDataGrid.Visibility = Visibility.Visible;
            try
            {
                infoListBox.Items.Add("- File checking...");
                int count = 0;
                OperationsWithFiles.CheckAndCreateFolder(strTempDir);
                if (!DownloadFileFromWebsite.DoWebRequest(strURL, strTempDir, ref strError))
                {
                    infoListBox.Items.Add("- Error: " + strError);
                }
                else infoListBox.Items.Add("- File checked!");
                DataTable tempFile = ImportFromExcel.ImportExcelFile(strTempFilePath);

                if ((OperationsWithFiles.GetHashFile(strFilePath) != (OperationsWithFiles.GetHashFile(strTempFilePath))))
                {
                    threatsDataGrid.ItemsSource = CompareFiles.UpdateTable(localFile, tempFile, ref count).DefaultView;
                    infoListBox.Items.Add("- Counts of change notes: " + count);
                }
                else infoListBox.Items.Add("- File already updated!");
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }
        }        

        private void ShowBtn_Click(object sender, RoutedEventArgs e)
        {
            fullInfoNameLbx.Visibility = Visibility.Hidden;
            threatsDataGrid.Visibility = Visibility.Visible;
            if (showInfoCheckBox.IsChecked == true)
            {
                threatsDataGrid.ItemsSource = CompareFiles.ViewTable(localFile).DefaultView;
                infoListBox.Items.Add("- Show information about threats!");
            }
            else MessageBox.Show("Please select!");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                DataTable tempDt = CompareFiles.ViewTable(localFile);
                threatsDataGrid.ItemsSource = PagingView.CustomPaging(1, tempDt).DefaultView;
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }
           
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                DataTable tempDt = CompareFiles.ViewTable(localFile);
                threatsDataGrid.ItemsSource = PagingView.CustomPaging(2, tempDt).DefaultView;
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }        
        }

        private void fullViewBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                fullInfoNameLbx.Items.Clear();
                fullInfoNameLbx.Visibility = Visibility.Visible;
                for (int i = 1; i < localFile.Rows.Count; i++)
                {
                    if (threatSearchCbx.Text == localFile.Rows[i][1].ToString())
                    {
                        fullInfoNameLbx.Items.Add("Идентификатор: " + "УБИ." + localFile.Rows[i][0].ToString().PadLeft(3, '0'));
                        fullInfoNameLbx.Items.Add("Наименование: " + localFile.Rows[i][1]);
                        fullInfoNameLbx.Items.Add("Описание: " + localFile.Rows[i][2]);
                        fullInfoNameLbx.Items.Add("Источник угрозы: " + localFile.Rows[i][3]);
                        fullInfoNameLbx.Items.Add("Объект воздействия: " + localFile.Rows[i][4]);
                        fullInfoNameLbx.Items.Add("Нарушение конфиденциальности: " + localFile.Rows[i][5]);
                        fullInfoNameLbx.Items.Add("Нарушение целостности: " + localFile.Rows[i][6]);
                        fullInfoNameLbx.Items.Add("Нарушение доступности: " + localFile.Rows[i][7]);
                        fullInfoNameLbx.Items.Add("Дата включения угрозы в БнД: " + localFile.Rows[i][8]);
                        fullInfoNameLbx.Items.Add("Дата последнего изменения: " + localFile.Rows[i][9]);
                    }
                }
                threatsDataGrid.Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }
            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            Directory.Delete(strTempDir);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Copy(strTempFilePath, strFilePath, overwrite: true);
                infoListBox.Items.Add("- File saved!");
            }
            catch (Exception ex)
            {
                infoListBox.Items.Add("- Error: " + ex);
            }
        }
    }
}

