﻿using System;
using System.Net;
using System.IO;
using System.Linq;


namespace DbSecurityThreatsParser
{
    public static class DownloadFileFromWebsite
    {
        public static bool DownloadFile(HttpWebResponse resp, string strFileName, ref string strError)
        {
            bool returnValue = true;
            try
            {
                long lngFileSize = resp.ContentLength;
                using (StreamReader fileIn = new StreamReader(resp.GetResponseStream()))
                {
                    using (StreamWriter fileOut = new StreamWriter(strFileName))
                    {
                        long totalBytesRead = 0;
                        int intBytesRead = 0;
                        byte[] buff = new byte[4096];
                        for (int intLoop = 0; totalBytesRead < lngFileSize; intLoop++)
                        {
                            intBytesRead = fileIn.BaseStream.Read(buff, 0, buff.Length);
                            fileOut.BaseStream.Write(buff, 0, intBytesRead);
                            totalBytesRead += intBytesRead;
                        }
                        fileOut.Flush();
                        fileOut.Close();
                    }
                    fileIn.Close();
                }
                resp.Close();
            }
            catch (Exception exc)
            {
                returnValue = false;
                strError = exc.Message;
            }
            return returnValue;
        }
        public static bool DoWebRequest(string strURL, string strFolder, ref string strError)
        {
            bool returnValue = true;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strURL);
                req.Method = WebRequestMethods.Http.Get;
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    string strFileName = strURL.Split('/').Last();
                    if (!DownloadFile(resp, Path.Combine(strFolder, strFileName), ref strError))
                    {
                        returnValue = false;
                    }
                    resp.Close();
                }
            }
            catch (Exception exc)
            {
                returnValue = false;
                strError = exc.Message;
            }
            return returnValue;            
        }
    }
}
