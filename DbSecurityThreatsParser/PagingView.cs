﻿using System;
using System.Collections.Generic;
using System.Data;

namespace DbSecurityThreatsParser
{
    static class PagingView
    {
        private static int pagingPageIndex = 1;
        private static int pagingNoOfRecPerPage = 15;
        private enum PagingMode { Next = 1, Previous = 2 };
        public static DataTable CustomPaging(int mode, DataTable dt)
        {
            int totalRecords = dt.Rows.Count;
            int pageSize = pagingNoOfRecPerPage;
            switch (mode)
            {
                case (int)PagingMode.Next:
                    if (totalRecords > (pagingPageIndex * pageSize))
                    {
                        DataTable tmpTable = new DataTable();
                        tmpTable = dt.Clone();
                        if (totalRecords >= ((pagingPageIndex * pageSize) + pageSize))
                        {
                            for (int i = pagingPageIndex * pageSize;
                                 i < ((pagingPageIndex * pageSize) + pageSize); i++)
                            {
                                tmpTable.ImportRow(dt.Rows[i]);
                            }
                        }
                        else
                        {
                            for (int i = pagingPageIndex * pageSize; i < totalRecords; i++)
                            {
                                tmpTable.ImportRow(dt.Rows[i]);
                            }
                        }
                        pagingPageIndex += 1;

                        return tmpTable;
                    }
                    break;
                case (int)PagingMode.Previous:
                    if (pagingPageIndex > 1)
                    {
                        DataTable tmpTable = new DataTable();
                        tmpTable = dt.Clone();
                        pagingPageIndex -= 1;

                        for (int i = ((pagingPageIndex * pageSize) - pageSize);
                            i < (pagingPageIndex * pageSize); i++)
                        {
                            tmpTable.ImportRow(dt.Rows[i]);
                        }
                        return tmpTable;
                    }
                    break;
            }
            return null;
        }
    }
}
