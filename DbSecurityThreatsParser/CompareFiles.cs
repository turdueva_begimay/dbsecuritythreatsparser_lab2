﻿using System;
using System.Data;


namespace DbSecurityThreatsParser
{
    static class CompareFiles
    {
        
        public static DataTable UpdateTable(DataTable localTable, DataTable tempTable, ref int count )
        {
            DataTable table = new DataTable();
            DataColumn idThreadColumn = new DataColumn("ID", Type.GetType("System.String"));
            DataColumn beforeColumn = new DataColumn("Before", Type.GetType("System.String"));
            DataColumn afterColumn = new DataColumn("After", Type.GetType("System.String"));
            table.Columns.Add(idThreadColumn);
            table.Columns.Add(beforeColumn);
            table.Columns.Add(afterColumn);
            DataRow row = table.NewRow();
            for (int i = 0; i < localTable.Rows.Count; i++)
            {
                for (int j = 0; j < localTable.Columns.Count; j++)
                {
                    if (!(tempTable.Rows[i][j].ToString() == localTable.Rows[i][j].ToString()))
                    {
                        count++;
                        table.Rows.Add(new object[] { tempTable.Rows[i][0].ToString().PadLeft(3, '0'), tempTable.Rows[i][j].ToString(), localTable.Rows[i][j].ToString() });
                    }
                }                
            }
            return table;        
        }

        public static DataTable ViewTable(DataTable localTable)
        {
            DataTable table = new DataTable();
            DataColumn idThreadColumn = new DataColumn("ID", Type.GetType("System.String"));
            DataColumn nameColumn = new DataColumn("Name", Type.GetType("System.String"));
            table.Columns.Add(idThreadColumn);
            table.Columns.Add(nameColumn);
            DataRow row = table.NewRow();
            for (int i = 1; i < localTable.Rows.Count; i++)
            {
                table.Rows.Add(new object[] { "УБИ." + localTable.Rows[i][0].ToString().PadLeft(3, '0'), localTable.Rows[i][1].ToString() });
            }
            return table;
        }


    }
}
