﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace DbSecurityThreatsParser
{
    public static class OperationsWithFiles
    {        
        public static bool CheckExistFile(string filePath)
        {
           if( Directory.GetFiles(filePath, "*.xlsx").Any())
            {
                return true;
            }
           return false;
        }

       
        public static string CheckAndCreateFolder(string folderTempPath)
        {
            bool folderExists = Directory.Exists(folderTempPath);
            if (!folderExists)
                return Directory.CreateDirectory(folderTempPath).FullName;
            else return "";            
        }

        public static string GetHashFile(string fileName)
        {
            using (FileStream fs = File.OpenRead(fileName))
            {
                MD5 md5 = MD5.Create();
                StringBuilder sb = new StringBuilder();
                foreach (byte b in md5.ComputeHash(fs))
                {
                    sb.Append(b.ToString());
                }
                return sb.ToString();
            }
        }


    }
}
