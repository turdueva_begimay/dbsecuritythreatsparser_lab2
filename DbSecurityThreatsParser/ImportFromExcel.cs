﻿using System.Data;
using ClosedXML.Excel;
using System.Linq;

namespace DbSecurityThreatsParser
{
    public static class ImportFromExcel
    {
        public static DataTable ImportExcelFile(string fileNamePath)
        {

            using (XLWorkbook workbook = new XLWorkbook(fileNamePath))
            {
                IXLWorksheet worksheet = workbook.Worksheet(1);
                DataTable dt = new DataTable();
                bool firstRow = true;
                foreach (IXLRow row in worksheet.Rows())
                {
                    if (firstRow)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            dt.Columns.Add(cell.Value.ToString());
                        }
                        firstRow = false;
                    }
                    else
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (IXLCell cell in row.Cells())
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                            i++;
                        }
                    }
                }
                return dt;
            }
        }
    }
}
